package com.ronanw.connect5client;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.lang.reflect.*;

public class BoardTest extends TestCase {
    private Board board;

    @Override
    protected void setUp() throws Exception {
        board = new Board();
    }

    public BoardTest(String testName) {
        super(testName);
    }

    public static Test suite() {
        return new TestSuite(BoardTest.class);
    }

    public void testBoard() throws Exception {
        Field field = board.getClass().getDeclaredField("board");
        field.setAccessible(true);
        char[][] value = (char[][]) field.get(board);
        assertNotNull(value);
    }

    public void testSetMark() throws Exception {
        int row = 2;
        int column = 3;
        char mark = 'O';
        board.setMark(row, column, mark);
        Field field = board.getClass().getDeclaredField("board");
        field.setAccessible(true);
        char[][] value = (char[][]) field.get(board);
        assertEquals(value[row][column], mark);
    }

    public void testSetMyColour() throws Exception {
        int myColour = 2;
        board.setMyColour(myColour);
        Field field = board.getClass().getDeclaredField("myColour");
        field.setAccessible(true);
        int value = (int) field.get(board);
        assertEquals(myColour, value);
    }

    public void testSetOpponentColour() throws Exception {
        int opponentColour = 2;
        board.setOpponentColour(opponentColour);
        Field field = board.getClass().getDeclaredField("opponentColour");
        field.setAccessible(true);
        int value = (int) field.get(board);
        assertEquals(opponentColour, value);
    }

    public void testSetMyMark() throws Exception {
        char myMark = 'O';
        board.setMyMark(myMark);
        Field field = board.getClass().getDeclaredField("myMark");
        field.setAccessible(true);
        char value = (char) field.get(board);
        assertEquals(myMark, value);
    }

    @Override
    protected void tearDown() throws Exception {
        board = null;
    }
}
