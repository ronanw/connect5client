package com.ronanw.connect5client;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.http.HttpClient;
import java.util.Scanner;

public class Connect5ClientTest extends TestCase {
    private Connect5Client connect5Client;

    @Override
    protected void setUp() throws Exception {
        connect5Client = new Connect5Client();
    }

    public Connect5ClientTest(String testName) {
        super(testName);
    }

    public static Test suite() {
        return new TestSuite(Connect5ClientTest.class);
    }

    public void testConstructor() throws Exception {
        Field field = connect5Client.getClass().getDeclaredField("board");
        field.setAccessible(true);
        Board board = (Board)field.get(connect5Client);
        assertNotNull(board);
        field = connect5Client.getClass().getDeclaredField("sc");
        field.setAccessible(true);
        Scanner sc = (Scanner)field.get(connect5Client);
        assertNotNull(sc);
    }

    public void testBegin() throws Exception {
        connect5Client.begin();
        Field field = connect5Client.getClass().getDeclaredField("httpClient");
        field.setAccessible(true);
        HttpClient httpClient = (HttpClient)field.get(connect5Client);
        assertNotNull(httpClient);
    }

    public void testGetPlayerName() throws Exception {
        String testName = "player";
        Method method = connect5Client.getClass().getDeclaredMethod("getPlayerName", Scanner.class);
        method.setAccessible(true);

        String name = (String)method.invoke(connect5Client, new Scanner(testName));
        assertEquals(testName, name);
    }

    public void testGetInput() throws Exception {
        String testLocation = "1";

        Method method = connect5Client.getClass().getDeclaredMethod("getInput", Scanner.class);
        method.setAccessible(true);

        String location = (String)method.invoke(connect5Client, new Scanner(testLocation));
        assertEquals(testLocation, location);
    }

    @Override
    protected void tearDown() throws Exception {
        connect5Client = null;
    }
}
