package com.ronanw.connect5client;

import java.util.HashMap;

public class Board {
    public static final char X_MARK = 'X', O_MARK = 'O';
    private char[][] board;
    private char myMark;
    private final int HEIGHT = 6;
    private final int WIDTH = 9;
    private final HashMap<Integer, String> colourHashMap;
    private int myColour, opponentColour;

    private final String RED = "\033[31m";
    private final String GREEN = "\033[32m";
    private final String YELLOW = "\033[33m";
    private final String BLUE = "\033[34m";
    private final String WHITE = "\033[37m";
    private final String RESET = "\033[0m";

    public Board() {
        board = new char[HEIGHT][WIDTH];
        colourHashMap = new HashMap<>();
        setupColourHashMap();
    }

    private void setupColourHashMap() {
        colourHashMap.put(1, RED);
        colourHashMap.put(2, GREEN);
        colourHashMap.put(3, YELLOW);
        colourHashMap.put(4, BLUE);
        colourHashMap.put(5, WHITE);
    }

    public void setMyColour(int myColour) {
        this.myColour = myColour;
    }

    public void setOpponentColour(int opponentColour) {
        this.opponentColour = opponentColour;
    }

    public void setMyMark(char myMark) {
        this.myMark = myMark;
    }

    public void printBoard() {
        for (int row = 0; row < board.length; row++) {
            for (int column = 0; column < board[row].length; column++) {
                char contents = board[row][column];
                if (!Character.isLetter(contents)) {
                    contents = ' ';
                }
                String colour = contents == myMark ?
                                colourHashMap.get(myColour) :
                                colourHashMap.get(opponentColour);
                if (colour == null) {
                    // Better to have nothing than "null" printed on the board
                    colour = "";
                }
                System.out.print("[" + colour + contents + RESET + "]");
            }
            System.out.println();
        }
    }

    public void setMark(int row, int column, char mark) {
        board[row][column] = mark;
    }
}