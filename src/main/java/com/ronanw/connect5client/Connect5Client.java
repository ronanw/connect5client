package com.ronanw.connect5client;

import java.net.URI;
import java.io.IOException;
import java.net.http.HttpClient;
import java.net.http.HttpHeaders;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.Scanner;

public class Connect5Client implements Runnable {
    private HttpClient httpClient;
    private Board board;
    private char myMark;
    private String playerName;
    private String opponentName;
    private boolean myTurn;
    private boolean gameStarted;
    private Scanner sc;
    private final String SERVER_ADDRESS = "http://localhost:8080/connect5";
    private final String PLAYER_ADDRESS = "http://localhost:8080/player";

    public static void main(String[] args) {
        Connect5Client Connect5Client = new Connect5Client();
        Connect5Client.begin();
    }

    public Connect5Client() {
        // This hook runs on reception of signals such as SIGTERM and SIGKILL
        // The server will be notified on the disconnection and the process will end
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                notifyDisconnection();
            }
        });

        board = new Board();
        sc = new Scanner(System.in, "UTF-8");
        clearScreen();
    }

    public void begin() {
        httpClient = HttpClient.newBuilder().version(HttpClient.Version.HTTP_2).build();

        Thread gameRunner = new Thread(this);
        gameRunner.start();
    }

    private String getPlayerName() {
        return getPlayerName(sc);
    }

    // The scanner is injected to this method to allow unit testing
    private String getPlayerName(Scanner scanner) {
        System.out.print("Please enter your name: ");
        String name = scanner.nextLine();
        if (name.isEmpty()) {
            System.out.print("Name must not be empty, ");
            return getPlayerName();
        }
        return name;
    }

    private String getPlayerColour() {
        return getPlayerColour(sc);
    }

    // The scanner is injected to this method to allow unit testing
    private String getPlayerColour(Scanner scanner) {
        String input = scanner.nextLine();
        try {
            int number = Integer.parseInt(input);
            if (number < 1 || number > 5) {
                return handleColourError();
            }
            board.setMyColour(number);
        } catch (NumberFormatException nfe) {
            return handleColourError();
        }
        return input;
    }

    private String handleColourError() {
        System.out.print("You must enter a number between 1-5: ");
        return getPlayerColour();
    }

    public void run() {
        playerName = getPlayerName();
        System.out.println("1. Red");
        System.out.println("2. Green");
        System.out.println("3. Yellow");
        System.out.println("4. Blue");
        System.out.println("5. White");
        System.out.print("Please the number representing your colour: ");
        String colour = getPlayerColour();

        // Server is informed of client's request to start a game
        // The process will exit if the server responds that a game is already in progress
        sendStartRequest(colour);

        // The first player will receive an 'X', second player receives an 'O'
        System.out.println("Welcome " + playerName + ". Your mark is \"" + myMark + "\"");
        gameStarted = true;
        board.setMyMark(myMark);
        myTurn = myMark == Board.X_MARK;

        if (myTurn) {
            System.out.println("Waiting for the other player to join");
            waitForOtherPlayer();
        }
        while (true) {
            runGame();
        }
    }

    private void clearScreen() {
        // ANSI codes to clear the screen and place the terminal at row 0, column 0 for
        // cleaner output
        System.out.print("\033[2J");
        System.out.print("\033[0;0H");
    }

    private void sendStartRequest(String colour) {
        // The client communicates with the /connect5 endpoint of the server for non-game logic
        // such as starting, exiting and waiting for their turn
        HttpRequest request = HttpRequest.newBuilder().GET()
            .uri(URI.create(SERVER_ADDRESS))
            .setHeader(Constants.REQUEST_TO_START, "")
            .setHeader(Constants.PLAYER_NAME, playerName)
            .setHeader(Constants.PLAYER_COLOUR, colour)
            .build();

        HttpHeaders headers = sendRequest(request);
        headers.map().forEach((k, v) -> {
            String value = v.get(0);
            if (k.equals(Constants.PLAYER_MARK)) {
                myMark = value.charAt(0);
                if (myMark == Board.O_MARK) {
                    opponentName = headers.map().get(Constants.OPPONENT_NAME).get(0);
                    String opponentColour = headers.map().get(Constants.OPPONENT_COLOUR).get(0);
                    int opponentColourInt = Integer.parseInt(opponentColour.trim());
                    board.setOpponentColour(opponentColourInt);
                }
            } else if (k.equals(Constants.GAME_IN_PROGRESS)) {
                System.out.println("Game already in progress");
                System.exit(0);
            }
        });
    }

    private void waitForOtherPlayer() {
        HttpRequest request = HttpRequest.newBuilder().GET()
            .uri(URI.create(SERVER_ADDRESS))
            .setHeader(Constants.WAITING_FOR_OPPONENT, "")
            .build();

        HttpHeaders headers = sendRequestAsync(request);
        headers.map().forEach((k, v) -> {
            String value = v.get(0);
            if (k.equals(Constants.OPPONENT_NAME)) {
                opponentName = value;
                System.out.println("Opponent's name is " + opponentName);
            }
            if (k.equals(Constants.OPPONENT_COLOUR)) {
                String opponentColour = value;
                int opponentColourInt = Integer.parseInt(opponentColour.trim());
                board.setOpponentColour(opponentColourInt);
            }
        });
    }

    private HttpHeaders sendRequestAsync(HttpRequest request) {
        HttpHeaders headers = null;
        try {
            CompletableFuture<HttpResponse<String>> response =
                httpClient.sendAsync(request, HttpResponse.BodyHandlers.ofString());

            headers = response.thenApply(HttpResponse::headers).get();
        } catch (ExecutionException | InterruptedException ee) {
            System.out.println("Error sending Async Http request");
            ee.printStackTrace();
            System.exit(1);
        }

        return headers;
    }

    private HttpHeaders sendRequest(HttpRequest request) {
        HttpHeaders headers = null;
        try {
            HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
            if (response.statusCode() != 200) {
                System.out.println("Received error " + response.statusCode());
                System.exit(1);
            }
            headers = response.headers();
        } catch (InterruptedException | IOException ee) {
            System.out.println("Error sending Http request");
            ee.printStackTrace();
            System.exit(1);
        }

        return headers;
    }

    public void notifyDisconnection() {
        if (!gameStarted) {
            return;
        }
        try {
            HttpRequest request = HttpRequest.newBuilder().GET()
                .uri(URI.create(SERVER_ADDRESS))
                .setHeader(Constants.NOTIFY_DISCONNECTION, "" + myMark)
                .build();

            CompletableFuture<HttpResponse<String>> response = httpClient.sendAsync(request,
                    HttpResponse.BodyHandlers.ofString());

            response.get(1, TimeUnit.SECONDS);
        } catch (Exception ee) {
            // No operation, we are exiting anyway
        } finally {
            Runtime.getRuntime().halt(0);
        }
    }

    private void waitForTurn() {
        System.out.println("Waiting for turn...");
        HttpRequest request = HttpRequest.newBuilder().GET()
            .uri(URI.create(SERVER_ADDRESS))
            .setHeader(Constants.WAITING_FOR_TURN, myMark + "")
            .build();

        // The response to this request will be sent when the other player makes a move
        // or if they disconnect
        HttpHeaders headers = sendRequestAsync(request);
        headers.map().forEach((k, v) -> {
            String value = v.get(0);
            if (k.equals(Constants.OPPONENT_MOVE)) {
                int row = Character.getNumericValue(value.charAt(0));
                int column = Character.getNumericValue(value.charAt(2));
                clearScreen();
                System.out.println(opponentName + " has made a move");
                char mark = (myMark == Board.X_MARK) ? Board.O_MARK : Board.X_MARK;
                board.setMark(row, column, mark);
                board.printBoard();
                if (headers.map().containsKey(Constants.INFORM_OPPONENT_WIN)) {
                    System.out.println(opponentName + " has won");
                    System.exit(0);
                }
            } else if (k.equals(Constants.NOTIFY_DISCONNECTION)) {
                System.out.println(opponentName + " has exited the game");
                System.exit(0);
            }
        });
    }

    private String getInput() {
        return getInput(sc);
    }

    private String getInput(Scanner scanner) {
        String input = scanner.nextLine();
        try {
            int location = Integer.parseInt(input);
            if (location < 1 || location > 9) {
                return handleInputError();
            }
        } catch (NumberFormatException nfe) {
            return handleInputError();
        }
        return input;
    }

    private String handleInputError() {
        System.out.print("You must enter a number between 1-9: ");
        return getInput();
    }

    private void runGame() {
        if (!myTurn) {
            waitForTurn();
        }
        System.out.print("It's your turn " + playerName + ", please enter column (1-9): ");
        String location = getInput();

        // Communication of game logic is carried out between the /player + mark
        // endpoint of the server
        HttpRequest request = HttpRequest.newBuilder().GET()
            .uri(URI.create(PLAYER_ADDRESS + myMark))
            .setHeader(Constants.LOCATION, location)
            .build();
        HttpHeaders headers = sendRequest(request);
        headers.map().forEach((k, v) -> {
            String value = v.get(0);
            if (k.equals(Constants.VALID_MOVE)) {
                clearScreen();
                if (!headers.map().containsKey(Constants.INFORM_WIN)) {
                    System.out.println("Valid move, please wait");
                }
                myTurn = false;
                int row = Integer.parseInt(value);
                int column = Integer.parseInt(location) - 1;
                board.setMark(row, column, myMark);
                board.printBoard();
                if (headers.map().containsKey(Constants.INFORM_WIN)) {
                    System.out.println("You have won. Congratulations");
                    System.exit(0);
                }
            } else if (k.equals(Constants.INVALID_MOVE)) {
                System.out.println("Invalid move, try again");
                myTurn = true;
                runGame();
            } else if (k.equals(Constants.NOTIFY_DISCONNECTION)) {
                System.out.println(opponentName + " has exited the game");
                System.exit(0);
            }
        });
    }
}